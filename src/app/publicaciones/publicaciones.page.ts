import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Poesias } from '../model/Poesia';
import { PublicacionService } from '../services/publicacion.service';

@Component({
  selector: 'app-publicaciones',
  templateUrl: './publicaciones.page.html',
  styleUrls: ['./publicaciones.page.scss'],
})
export class PublicacionesPage implements OnInit {

  constructor(private poesiaService:PublicacionService,public router: Router,private route: ActivatedRoute) { }
  poesia:Poesias = new Poesias()
  nombre:string
  ngOnInit() {
  }
  guardar(){
    this.nombre = this.route.snapshot.paramMap.get('nombre');
    console.log("obtengo ", this.nombre);

    console.log(this.poesia)
    this.poesia.autor=this.nombre
   
    this.poesiaService.savePoesia(this.poesia);
    let navigationExtras: NavigationExtras={
    queryParams:{
    
      poesia:this.poesia
    }
  };
  
  //dirigise a otra pagina y pasarle los parametros
  this.router.navigate(['/listar-publicaciones-loguin']);
  
  }
}
