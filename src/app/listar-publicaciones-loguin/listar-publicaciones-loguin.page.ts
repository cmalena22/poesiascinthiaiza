import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Poesias } from '../model/Poesia';
import { PublicacionService } from '../services/publicacion.service';

@Component({
  selector: 'app-listar-publicaciones-loguin',
  templateUrl: './listar-publicaciones-loguin.page.html',
  styleUrls: ['./listar-publicaciones-loguin.page.scss'],
})
export class ListarPublicacionesLoguinPage implements OnInit {
  poesia: Observable< any []>;
  comentario:string
  uid:string
  po:  Poesias= new Poesias()
  constructor(private poesiaService:PublicacionService,public router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
    this.poesia=this.poesiaService.getPoesia();
  }

  publicar(come:string, uid:string){
         console.log(come)
         this.poesiaService.getPoesiaById2(uid).subscribe(data => {
          const aux:any = data
          this.po = aux[0];
          this.po.comentario=come
         console.log(this.po.comentario)
      });
      
    
  }

}
