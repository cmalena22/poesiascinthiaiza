import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListarPublicacionesLoguinPageRoutingModule } from './listar-publicaciones-loguin-routing.module';

import { ListarPublicacionesLoguinPage } from './listar-publicaciones-loguin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListarPublicacionesLoguinPageRoutingModule
  ],
  declarations: [ListarPublicacionesLoguinPage]
})
export class ListarPublicacionesLoguinPageModule {}
