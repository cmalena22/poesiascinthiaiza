import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListarPublicacionesLoguinPage } from './listar-publicaciones-loguin.page';

const routes: Routes = [
  {
    path: '',
    component: ListarPublicacionesLoguinPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListarPublicacionesLoguinPageRoutingModule {}
