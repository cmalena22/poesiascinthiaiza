import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListarPublicacionesLoguinPage } from './listar-publicaciones-loguin.page';

describe('ListarPublicacionesLoguinPage', () => {
  let component: ListarPublicacionesLoguinPage;
  let fixture: ComponentFixture<ListarPublicacionesLoguinPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarPublicacionesLoguinPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListarPublicacionesLoguinPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
