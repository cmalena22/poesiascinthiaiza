import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Usuario } from '../model/Usuario';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.page.html',
  styleUrls: ['./inicio-sesion.page.scss'],
})
export class InicioSesionPage implements OnInit {
  correo:string
  contrasena:string
  usuario: Usuario= new Usuario();
  constructor(private usuarioService:UsuarioService,public router: Router) { }

  ngOnInit() {
  }
  loguin() {
    this.usuarioService.getContactoById2(this.correo).subscribe(data => {
      console.log(data)
      const aux:any = data
      this.usuario = aux[0];
      if(this.correo==this.usuario.correo && this.contrasena==this.usuario.contrasena)
      {
        this.router.navigate(['/publicaciones/'+this.usuario.nombre]);
     
      }else{
        console.log("no")
      }
    
  });


  }
}
