import { core } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Usuario } from '../model/Usuario';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  constructor(public afs:AngularFirestore) { }

  saveRopa(usuario : Usuario){//
    const refropa=this.afs.collection("usuario");
    if(usuario.uid==null)
    usuario.uid=this.afs.createId();
    refropa.doc(usuario.uid).set(Object.assign({},usuario),{merge:true})


  }

  
  getRopa(): Observable< any []>{
    return this.afs.collection("usuario").valueChanges();

  }

  getContactoById2(correo: string) :Observable<any>{
    return this.afs.collection("usuario", 
            ref => ref.where('correo', '==', correo))
                      .valueChanges();
  }




}
