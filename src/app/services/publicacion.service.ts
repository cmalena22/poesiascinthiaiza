import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Poesias } from '../model/Poesia';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {

  constructor(public afs:AngularFirestore) { }

  savePoesia(poesia : Poesias){//
    const refpoesia=this.afs.collection("poesia");
    if(poesia.uid==null)
    poesia.uid=this.afs.createId();
    refpoesia.doc(poesia.uid).set(Object.assign({},poesia),{merge:true})


  }
  getPoesia(): Observable< any []>{
    return this.afs.collection("poesia").valueChanges();

    
  }


  getPoesiaById2(uid: string) :Observable<any>{
    return this.afs.collection("poesia", 
            ref => ref.where('uid', '==', uid))
                      .valueChanges();
  }


}
