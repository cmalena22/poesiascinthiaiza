import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Usuario } from '../model/Usuario';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-registrar-usuario',
  templateUrl: './registrar-usuario.page.html',
  styleUrls: ['./registrar-usuario.page.scss'],
})
export class RegistrarUsuarioPage implements OnInit {
  usuario : Usuario = new Usuario();
  constructor(public router: Router,
    public usuarioService:UsuarioService,private route: ActivatedRoute) { 
      
    }

  ngOnInit() {
  }
guardar(){
  console.log(this.usuario)
  this.usuarioService.saveRopa(this.usuario);
  let navigationExtras: NavigationExtras={
  queryParams:{
    usuario:this.usuario
  }
};

//dirigise a otra pagina y pasarle los parametros
//this.router.navigate(['/lista-ropa']);

}

}
