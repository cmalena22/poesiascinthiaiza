import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListarPublicacionesPageRoutingModule } from './listar-publicaciones-routing.module';

import { ListarPublicacionesPage } from './listar-publicaciones.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListarPublicacionesPageRoutingModule
  ],
  declarations: [ListarPublicacionesPage]
})
export class ListarPublicacionesPageModule {}
