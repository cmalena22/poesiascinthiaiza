import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListarPublicacionesPage } from './listar-publicaciones.page';

const routes: Routes = [
  {
    path: '',
    component: ListarPublicacionesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListarPublicacionesPageRoutingModule {}
