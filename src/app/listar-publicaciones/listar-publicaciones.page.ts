import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PublicacionService } from '../services/publicacion.service';

@Component({
  selector: 'app-listar-publicaciones',
  templateUrl: './listar-publicaciones.page.html',
  styleUrls: ['./listar-publicaciones.page.scss'],
})
export class ListarPublicacionesPage implements OnInit {
  poesia: Observable< any []>;
  constructor(public poesiaService:PublicacionService, public router:Router) { }

  ngOnInit() {
    this.poesia=this.poesiaService.getPoesia();
  }
publicar(poesia){
  
  this.poesia.forEach(poesia =>{
    //Aquí insertas el código de comparación que necesites
    console.log(poesia)
});
}
}
