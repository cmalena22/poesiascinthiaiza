import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListarPublicacionesPage } from './listar-publicaciones.page';

describe('ListarPublicacionesPage', () => {
  let component: ListarPublicacionesPage;
  let fixture: ComponentFixture<ListarPublicacionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarPublicacionesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListarPublicacionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
